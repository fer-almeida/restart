#include <stdio.h>
#include <string.h>

#include "version.h"

#define LANGUAGE	ENGLISH
#include "res.h"

void restart_pc(void) {
	asm {
		db 0x0EA
		dw 0x0000
		dw 0xFFFF
	}
}

void help_message(void) {
	printf("%s", HELP_MSG);
}

void version_message(void) {
	printf("%s - %s v%d.%02d\r\n", PRODUCT, COMMAND, MAJOR_VERSION, MINOR_VERSION);
}

int main(int argc, char *argv[]) {
	if(strstr(argv[0], COMMAND) == NULL) {
		printf("%s\r\n", ERRORCMD_MSG);
		return -1;
	}

	if(argc == 1) {
		printf("%s\r\n", RESTARTING_MSG);
		restart_pc();
	} else {
		if(strcmp(argv[1], "/?") == 0) { help_message(); return 0; }
		if(strcmp(argv[1], "/v") == 0 || strcmp(argv[1], "/V") == 0) { version_message(); return 0; }
		if(strcmp(argv[1], "/s") == 0 || strcmp(argv[1], "/S") == 0) { restart_pc(); return 0; }

		printf("%s - %s\r\n", INVALIDSW_MSG, argv[1]);
	}
	return 0;
}