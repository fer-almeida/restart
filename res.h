#ifndef _RES_H
#define _RES_H

// English
#define ENGLISH                 1
#define EN_RESTARTING_MSG       "Restarting PC..."
#define EN_HELP_MSG             "Soft-restart the computer, equivalent as pressing CTRL+ALT+DEL keys.\r\n\r\nRESTART [/V] [/S]\r\n\r\n  /V      Show version information.\r\n  /S      Silent mode - Does not show restarting message.\r\n"
#define EN_INVALIDSW_MSG        "Invalid switch"
#define EN_ERRORCMD_MSG         "Bad command or file name"

// Portuguese
#define PORTUGUESE              2
#define PT_RESTARTING_MSG       "Reiniciando o PC..."
#define PT_HELP_MSG             "Reinicia o computador, equivalente a pressionar as teclas CTRL+ALT+DEL.\r\n\r\nRESTART [/V] [/S]\r\n\r\n  /V      Exibe informacoes da versao.\r\n  /S      Modo silencioso - Nao exibe a mensagem de reiniciar.\r\n"
#define PT_INVALIDSW_MSG        "Parametro invalido"
#define PT_ERRORCMD_MSG         "Comando ou nome de arquivo invalido"

// Spanish
#define SPANISH                 3
#define SP_RESTARTING_MSG       "Al reiniciar el PC..."
#define SP_HELP_MSG             "Reinicia el equipo, lo que equivale a pulsar CTRL+ALT+DEL.\r\n\r\nRESTART [/V] [/S]\r\n\r\n  /V      Mostrar informacion de version.\r\n  /S      Modo silencioso - No muestra el mensage de reinicio.\r\n"
#define SP_INVALIDSW_MSG        "Parametro invalido"
#define SP_ERRORCMD_MSG         "Comando o nombre de archivo erroneo"

// French
#define FRENCH                  4
#define FR_RESTARTING_MSG       "Redemarrage du PC..."
#define FR_HELP_MSG             "Redemarrez l'ordinateur, ce qui equivault a appuyer sur les CTRL+ALT+DEL.\r\n\r\nRESTART [/V] [/S]\r\n\r\n  /V      Afficher les informations de version.\r\n  /S      Mode silencieux - Ne mostre pas le message de redemarrage.\r\n"
#define FR_INVALIDSW_MSG        "Parametre invalide"
#define FR_ERRORCMD_MSG         "Mauvaise commande ou nom de fichier"

// German
#define GERMAN			5
#define GR_RESTARTING_MSG       "Neustarten PC..."
#define GR_HELP_MSG             "Starten Sie den Computer neu, wenn Sie die CTRL+ALT+DEL drucken.\r\n\r\nRESTART [/V] [/S]\r\n\r\n  /V      Versionsinformationen anzeigen.\r\n  /S      Hudemodus - Zeigt keine Neustartmeldung an.\r\n"
#define GR_INVALIDSW_MSG        "Ungultiger Parameter"
#define GR_ERRORCMD_MSG         "Fehlerhafter Befel oder Dateiname"

// String Table
#if   LANGUAGE == ENGLISH
	#define RESTARTING_MSG  EN_RESTARTING_MSG
	#define HELP_MSG        EN_HELP_MSG
	#define INVALIDSW_MSG   EN_INVALIDSW_MSG
	#define ERRORCMD_MSG    EN_ERRORCMD_MSG
#elif LANGUAGE == PORTUGUESE
	#define RESTARTING_MSG  PT_RESTARTING_MSG
	#define HELP_MSG        PT_HELP_MSG
	#define INVALIDSW_MSG   PT_INVALIDSW_MSG
	#define ERRORCMD_MSG    PT_ERRORCMD_MSG
#elif LANGUAGE == SPANISH
	#define RESTARTING_MSG  SP_RESTARTING_MSG
	#define HELP_MSG        SP_HELP_MSG
	#define INVALIDSW_MSG   SP_INVALIDSW_MSG
	#define ERRORCMD_MSG    SP_ERRORCMD_MSG
#elif LANGUAGE == FRENCH
	#define RESTARTING_MSG  FR_RESTARTING_MSG
	#define HELP_MSG        FR_HELP_MSG
	#define INVALIDSW_MSG   FR_INVALIDSW_MSG
	#define ERRORCMD_MSG    FR_ERRORCMD_MSG
#elif LANGUAGE == GERMAN
	#define RESTARTING_MSG  GR_RESTARTING_MSG
	#define HELP_MSG        GR_HELP_MSG
	#define INVALIDSW_MSG   GR_INVALIDSW_MSG
	#define ERRORCMD_MSG    GR_ERRORCMD_MSG
#endif

#endif
